#!/usr/bin/env python
# coding: utf-8

"""   # Given the string:
      s = 'prasetiya mulya'
      # Use indexing to print out the following:
"""

# In[2]:


s = 'prasetiya mulya'


# In[5]:


s[10:11]


# In[30]:


s[0:3]


# In[10]:


s[3:5]+s[10:12]


# In[ ]:


# String 'jan' tidak terdapat pada s


# In[35]:


s[:12:-1]+s[11]+s[10]+s[6]


# In[44]:


s[::-2]

