#!/usr/bin/env python
# coding: utf-8

# In[29]:


# Given a string, return a string where for every char in the original,
# there are three chars.
# tripleChar('The') → 'TTThhheee'
# tripleChar('AAbb') → 'AAAAAAbbbbbb'
# tripleChar('Hi-There') → 'HHHiii---TTThhheeerrree'


# In[30]:


def tripleChar(str):
    for i in str:
        print(i*3,end = "")

tripleChar('Daus')

