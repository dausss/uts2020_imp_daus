#!/usr/bin/env python
# coding: utf-8

"""  # Given this nested list:
     l = [3,7,[1,4,['hello','world']]]
     # Reassign "hello" to be "goodbye"
"""

# In[3]:


l = [3,7,[1,4,['Hello','World']]]


# In[9]:


l[2][2][0] = 'Goodbye'
print(l)

