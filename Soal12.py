#!/usr/bin/env python
# coding: utf-8
""" 
# 1. The computer will think of 3 digit number that has no repeating
digits.
# 2. You will then guess a 3 digit number
# 3. The computer will then give back clues, the possible clues are:
#
# Close: You've guessed a correct number but in the wrong position
# Match: You've guessed a correct number in the correct position
# Nope: You haven't guess any of the numbers correctly
#
# 4. Based on these clues you will guess again until you break the code
with a
# perfect match!
# There are a few things you will have to discover for yourself for this
game!
# Here are some useful hints:
# Try to figure out what this code is doing and how it might be useful to
you
import random
digits = list(range(10))
random.shuffle(digits)
print(digits[:3])
# Another hint:
guess = input("What is your guess? ")
print(guess)
# Think about how you will compare the input to the random number, what
format
# should they be in? Maybe some sort of sequence? Watch the Lecture video
for more hints!
"""

# In[8]:


import random
digits = list(range(10))
random.shuffle(digits)
tebak = digits[:3]
print(tebak)


# In[9]:


tebakan = []
while tebak != tebakan:
    tebakan = []
    print('Masukkan tebakan digit pertama: ')
    a = int(input())
    tebakan.insert(0,a)
    print('Masukkan tebakan digit kedua: ')
    b = int(input())
    tebakan.insert(1,b)
    print('Masukkan tebakan digit ketiga: ')
    c = int(input())
    tebakan.insert(2,c)
    
    if tebakan != tebak and (a in tebak) and (b in tebak) and (c in tebak):
        print("Tebakan anda hampir benar, tetapi berada dilokasi yang salah :')")
    elif tebakan != tebak:
        print('Jawaban Anda Salah ')
        break
    elif tebakan == tebak:
        print('Jawaban Anda Benar')


# In[ ]:




