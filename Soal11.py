#!/usr/bin/env python
# coding: utf-8

# In[ ]:


# Return the number of odd integers in the given array.
# Examples:
# count_odds([2, 1, 2, 3, 4]) → 2
# count_odds([2, 2, 0]) → 0
# count_odds(1, 3, 5]) → 3


# In[6]:


def count_odds(nums):
    a = 0
    for i in nums:
        if i % 2 != 0:
            a+=1
            print(a, end = '\r')
            
count_odds([1,1,1,1,1,1,1,1,1])

