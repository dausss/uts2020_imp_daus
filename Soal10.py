#!/usr/bin/env python
# coding: utf-8

# In[ ]:

"""
# Given 3 int values, a b c, return their sum. However, if any of the
values is a
# teen -- in the range 13-19 inclusive -- then that value counts as 0,
except 15
# and 16 do not count as a teens. Write a separate helper "def
fix_teen(n):"that
# takes in an int value and returns that value fixed for the teen rule.
# In this way, you avoid repeating the teen code 3 times (i.e.
"decomposition").
# Define the helper below and at the same indent level as the main
no_teen_sum().
# Again, you will have two functions for this problem!
# Examples:
# no_teen_sum(1, 2, 3) → 6
# no_teen_sum(2, 13, 1) → 3
# no_teen_sum(2, 1, 14) → 3
# def no_teen_sum(a, b, c):
 # CODE GOES HERE
# def fix_teen(n):
 # CODE GOES HERE
"""


# In[10]:


def no_teen_sum(a, b, c):
    return fix_teen(a) + fix_teen(b) + fix_teen(c)
    
def fix_teen(d):
    if 13 <= d and d <= 19 and d != 15 and d != 16:
        return 0    
    return d

no_teen_sum(1,2,3)


# In[11]:


no_teen_sum(1,2,13)


# In[12]:


no_teen_sum(1,2,14)

