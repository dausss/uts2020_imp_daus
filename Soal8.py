#!/usr/bin/env python
# coding: utf-8

# Given two strings, return True if either of the strings appears in the
# beginning of the other string, ignoring upper/lower case differences (in
# other words, the
# computation should not be "case sensitive").
# Note: s.lower() returns the lowercase version of a string.
# Examples:
# begin_other('abcKopi', 'abc') → True
# begin_other('AbC', 'HiaBc') → False
# begin_other('abc', 'abcXabc') → True
# begin_other('abc', 'abXabc') → False

def begin_end(a, b):
    a = a[::-1]
    b = b[::-1]
    a = a.lower()
    b = b.lower()
    return (b.endswith(a) or a.endswith(b))

begin_end('abcKopi', 'abc')


# In[7]:


begin_end('AbC', 'HiaBc') 


# In[8]:


begin_end('abc', 'abcXabc') 


# In[9]:


begin_end('abc', 'abXabc') 

