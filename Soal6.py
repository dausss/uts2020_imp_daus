#!/usr/bin/env python
# coding: utf-8

""" # Given a list of integers, return True if the sequence of numbers 7, 8, 9
    # appears in the list somewhere.
    # For example:
    # arrayCheck([1, 7, 8, 9, 1]) → True
    # arrayCheck([1, 9, 7, 8, 1]) → False
    # arrayCheck([1, 1, 2, 7, 8, 9]) → True
    def arrayCheck(nums):
    # CODE GOES HERE
"""

# In[39]:


def arrayCheck(num, a):
    return str(a)[1:-1] in str(num)

a = [7,8,9]


# In[41]:


arrayCheck([1,2,3,4,5,6,7,8,9],a)


# In[43]:


arrayCheck([1,2,3,4,5],a)

