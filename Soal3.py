#!/usr/bin/env python
# coding: utf-8

""" # Using keys and indexing, grab the 'hello' from the following
    dictionaries:
    d1 = {'simple_key':'hello'}
    d2 = {'k1':{'k2':'hello'}}
    d3 = {'k1':[{'nest_key':['this is deep',['annyeong','hello']]}]}
"""

# In[3]:


d1 = {'simple_key':'hello'}
d2 = {'k1':{'k2':'hello'}}
d3 = {'k1':[{'nest_key':['this is deep',['annyeong','hello']]}]}


# In[5]:


d1['simple_key']


# In[11]:


d2['k1']['k2']


# In[17]:


d3['k1'][0]['nest_key'][1][1]

