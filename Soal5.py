#!/usr/bin/env python
# coding: utf-8

""" # You are given two variables:
    age = 4
    name = "Sammy"
    money_in_pocket=100
    money_in_bank=500
    # Use print formatting to print the following string:
    "Hello my dog's name is Sammy and he is 4 years old"
    "Total Sammy’s money is 600 USD"
"""

# In[6]:


age = 4
name = "Sammy"
money_in_pocket= 100
money_in_bank= 500


# In[7]:


print("Hello my dog's name is {} and he is {} years old".format(name,age))
print("Total Sammy's money is {} USD".format(money_in_pocket+money_in_bank))

