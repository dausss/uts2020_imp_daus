#!/usr/bin/env python
# coding: utf-8

""" # Use a set to find the unique values of the list below:
    mylist = [1,1,1,1,1,2,2,2,2,3,3,3,3]
"""

# In[1]:


mylist = [1,1,1,1,1,2,2,2,2,3,3,3,3]


# In[2]:


set(mylist)

